
PREFIX=/usr
SAMTOOLS=/rsrch1/bcb/xfan3/pkg/samtools/samtools-0.1.19/

all:
	g++ -g -Wall -O2 -I${SAMTOOLS} main.cpp -o tigra-sv -lm -lz -L${SAMTOOLS} -lbam -lpthread
static:
	#g++ -g -Wall -O2 -I/gsc/pkg/bio/samtools/samtools-0.1.9/ tigra_sv.cpp -o tigra_sv_static -lm /gsc/pkg/bio/samtools/samtools-0.1.9/libbam.a /usr/lib/libz.a /usr/lib/gcc/x819_194-linux-gnu/4.2/libstdc++.a
	g++ -g -Wall -O2 -static-libgcc -L. -I${SAMTOOLS} main.cpp -o tigra-sv_static -Wl,-Bstatic -L${SAMTOOLS} -lbam -lz -Wl,-Bdynamic
debug:
	g++ -g -Wall -O1 -I${SAMTOOLS} main.cpp -o tigra-sv_test -lm -lz -L${SAMTOOLS} -lbam
clean:
	rm -f tigra-sv tigra-sv_test
install:
	install -D -m 755 tigra-sv $(DESTDIR)$(PREFIX)/bin/tigra-sv
